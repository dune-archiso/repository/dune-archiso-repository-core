## Frequently Asked Questions

- [MPICH Release 3.4.2](https://www.mpich.org/static/downloads/3.4.2/mpich-3.4.2-README.txt)
- [MPI Fortran support through the mpi_f08 module with gfortran](https://stackoverflow.com/q/65750862/9302545)
- [Compiler usage](https://www.hpc2n.umu.se/documentation/compilers/usage)
- [USING ENVIRONMENT MODULES](https://www.marquette.edu/high-performance-computing/modules.php)
- [Compiling and running MPI codes](https://wiki.unil.ch/ci/books/service-de-calcul-haute-performance-%28hpc%29/page/compiling-and-running-mpi-codes)
- [gfortran — the GNU Fortran compiler, part of GCC](https://gcc.gnu.org/wiki/GFortran)
- [Compiling Atomistic Codes](https://docs.hpc.wvu.edu/text/67.Atomistic.html)
- [Shifting toward C99](https://wiki.mpich.org/mpich/index.php/Shifting_toward_C99)
- [Compiling on Ubuntu- build- make- libtools unable to find path to mpich](https://oss.deltares.nl/web/delft3d/river1/-/message_boards/message/1815669)
- [Running UCX](https://openucx.readthedocs.io/en/master/running.html)
- [ARCHER2 development environment](https://epcced.github.io/2020-12-14-archer2-developers-online/03-develop-env/index.html)
  [](https://stackoverflow.com/a/14352330/9302545)
