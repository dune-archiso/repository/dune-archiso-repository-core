# [`dune-archiso-repository-core`](https://gitlab.com/dune-archiso/dune-archiso-repository-core) packages for Arch Linux

[![pipeline status](https://gitlab.com/dune-archiso/dune-archiso-repository-core/badges/main/pipeline.svg)](https://gitlab.com/dune-archiso/dune-archiso-repository-core/-/commits/main)
[![coverage report](https://gitlab.com/dune-archiso/dune-archiso-repository-core/badges/main/coverage.svg)](https://gitlab.com/dune-archiso/dune-archiso-repository-core/-/commits/main)

This is a third-party auto-updated repository with the following [tarballs](https://gitlab.com/dune-archiso/dune-archiso-repository-core/-/raw/main/packages.x86_64). Mostly packages required for DUNE modules, no DUNE modules found here.

## [Usage](https://gitlab.com/dune-archiso/repository/dune-archiso-repository-core/-/pipelines/latest)

### Add repository

1. Import GPG key from GPG servers.

```console
[user@hostname ~]$ sudo pacman-key --recv-keys 6225FD2615EB3DEE
[user@hostname ~]$ sudo pacman-key --finger 6225FD2615EB3DEE
[user@hostname ~]$ sudo pacman-key --lsign-key 6225FD2615EB3DEE
```

2. Append the following lines to `/etc/pacman.conf`:

```toml
[dune-archiso-repository-core]
SigLevel = Required DatabaseOptional
Server = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch
```

### List packages

To show actual packages list:

```console
[user@hostname ~]$ pacman -Sl dune-archiso-repository-core
```

### Install packages

To install package:

```console
[user@hostname ~]$ sudo pacman -Syu
[user@hostname ~]$ sudo pacman -S mpich
```

### `PKGBUILD`s from the [Arch User Repository](https://wiki.archlinux.org/title/Arch_User_Repository) 👀

- [`psurface`](https://aur.archlinux.org/pkgbase/psurface)
- [`adol-c`](https://aur.archlinux.org/packages/adol-c)
- [`alberta`](https://aur.archlinux.org/packages/alberta)

Powered by Arch Linux for Education (`arch4edu`).
