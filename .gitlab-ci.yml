###############################
# Setup
###############################

###############################
# Include the template scripts
###############################

include:
  - remote: "https://gitlab.com/dune-archiso/dune-archiso.gitlab.io/-/raw/main/templates/module.repo.yaml"
  - remote: "https://gitlab.com/dune-archiso/dune-archiso.gitlab.io/-/raw/main/templates/build.yml"
  - remote: "https://gitlab.com/dune-archiso/dune-archiso.gitlab.io/-/raw/main/templates/load.yml"
  - remote: "https://gitlab.com/dune-archiso/dune-archiso.gitlab.io/-/raw/main/templates/pages.yml"

#################################
# * Automatic Differentiation
#################################

adol-c:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    PACKAGE: "adol-c"
    ISDEPENDS: "true"
    DEPENDENCIES_PACKAGE: "colpack-*.pkg.tar.zst"
  needs: ["colpack"]

#################################
# Take it from arch4edu repo
# * Adaptive finite element toolbox
#################################

# alberta:
#   extends: .build_aur_template
#   tags: [saas-linux-medium-amd64]
#   stage: .pre
#   variables:
#     PACKAGE: "alberta"

#################################
# Build from local PKGBUILD
# * Adaptive finite element toolbox
#################################

# alberta-git:
#   extends: .build_aur_template
#   tags: [saas-linux-medium-amd64]
#   stage: .pre
#   variables:
#     ISAUR: "false"
#     MODULE: "alberta-git"
# DEPENDENCIES_PACKAGE: "duma-*.pkg.tar.zst electricfence-*.pkg.tar.zst"
# needs: ["duma", "electricfence"]

#################################
# * Arpack++ with patches
#################################

arpackpp:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    PACKAGE: "arpackpp"

#################################
# * CalculiX: 3D Structural Finite Element Program - Solver
#################################

calculix-ccx:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    PACKAGE: "calculix-ccx"
    ISDEPENDS: "true"
    DEPENDENCIES_PACKAGE: "spooles-*.pkg.tar.zst libsnl-svn-*.pkg.tar.zst"
  needs: ["spooles", "libsnl-svn"]

#################################
# * CalculiX to Paraview converter (frd to vtk/vtu)
#################################

ccx2paraview:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    PACKAGE: "ccx2paraview"

#################################
# Take it from arch4edu repo
# * Interior Point OPTimizer
#################################

# coin-or-ipopt:
#   extends: .build_aur_template
#   tags: [saas-linux-medium-amd64]
#   stage: .pre
#   variables:
#     PACKAGE: "coin-or-ipopt"

#################################
# * A Graph Coloring Algorithm Package
#################################

colpack:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    PACKAGE: "colpack"

#################################
# * The C++ interface of FEniCS, providing a consistent PSE (Problem Solving Environment) for ordinary and partial differential equations (stable)
#################################

dolfin:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    ISARCH4EDU: "true"
    PACKAGE: "dolfin"
    ISDEPENDS: "true"
    DEPENDENCIES_PACKAGE: "petsc-*.pkg.tar.zst python-dijitso-*.pkg.tar.zst python-fiat-*.pkg.tar.zst python-ufl-*.pkg.tar.zst python-ffc-*.pkg.tar.zst" # scotch-*.pkg.tar.zst
  needs: ["petsc", "python-dijitso", "python-fiat", "python-ufl", "python-ffc"]

#################################
# * Detect Unintended Memory Access
#################################

# duma:
#   extends: .build_aur_template
#   tags: [saas-linux-medium-amd64]
#   stage: .pre
#   variables:
#     PACKAGE: "duma"

#################################
# * A malloc(3) debugger
#################################

# electricfence:
#   extends: .build_aur_template
#   tags: [saas-linux-medium-amd64]
#   stage: .pre
#   variables:
#     PACKAGE: "electricfence"

#################################
# Build from local PKGBUILD
#################################

gstat:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    ISAUR: "false"
    MODULE: "gstat"
    ISDEPENDS: "true"
    DEPENDENCIES_PACKAGE: "meschach-*.pkg.tar.zst"
  needs: ["meschach"]

#################################
# Take it from arch4edu repo
# * Parallel solvers for sparse linear systems featuring multigrid methods
#################################

# hypre:
#   extends: .build_aur_template
#   tags: [saas-linux-medium-amd64]
#   stage: .pre
#   variables:
#     PACKAGE: "hypre"
#     ISDEPENDS: "true"
#     DEPENDENCIES_PACKAGE: "superlu_dist-*.pkg.tar.zst"
#   needs: ["superlu_dist"]

#################################
# * C++ performance portability programming ecosystem
#################################

kokkos:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    PACKAGE: "kokkos"

#################################
# * libSNL is a library of routines used for the manipulation of NURBS curves and surfaces
#################################

libsnl-svn:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    PACKAGE: "libsnl-svn"

#################################
# Build from local PKGBUILD
#################################

meschach:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    ISAUR: "false"
    MODULE: "meschach"

#################################
# Take it from arch4edu repo
# * A set of serial programs for partitioning graphs, partitioning finite element meshes, and producing fill reducing orderings for sparse matrices
#################################

# metis:
#   extends: .build_aur_template
#   tags: [saas-linux-medium-amd64]
#   stage: .pre
#   timeout: 2 minutes
#   variables:
#     PACKAGE: "metis"

#################################
# * Mesh generation component of FEniCS (stable)
#################################

mshr:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    ISARCH4EDU: "true"
    PACKAGE: "mshr"
    ISDEPENDS: "true"
    DEPENDENCIES_PACKAGE: "petsc-*.pkg.tar.zst python-dijitso-*.pkg.tar.zst python-fiat-*.pkg.tar.zst python-ufl-*.pkg.tar.zst python-ffc-*.pkg.tar.zst dolfin-*.pkg.tar.zst" # scotch-*.pkg.tar.zst
  needs:
    [
      "petsc",
      "python-dijitso",
      "python-fiat",
      "python-ufl",
      "python-ffc",
      "dolfin",
    ]

#################################
# Build from local PKGBUILD
# TODO: Server is down, consider use github.com/simunova/mtl4
#################################

# mtl4-svn:
#   extends: .build_aur_template
#   tags: [saas-linux-medium-amd64]
#   stage: .pre
#   variables:
#     ISAUR: "false"
#     MODULE: "mtl4-svn"

#################################
# Take it from arch4edu repo
# * A parallel graph partitioning library
#################################

# parmetis:
#   extends: .build_aur_template
#   tags: [saas-linux-medium-amd64]
#   stage: .pre
#   variables:
#     PACKAGE: "parmetis"

#################################
# * Parallel Graph Partitioning and Fill-reducing Matrix Ordering (git version)
#################################

parmetis-git:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    ISARCH4EDU: "true"
    PACKAGE: "parmetis-git"

#################################
# * Portable, extensible toolkit for scientific computation
#################################

petsc:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    PACKAGE: "petsc"
  before_script:
    - sudo sed -i 's/ccache check/ccache \!check/' /etc/makepkg.conf

#################################
# Take it from arch4edu repo
# * Portable, extensible toolkit for scientific computation (complex scalars)
#################################

# petsc-complex:
#   extends: .build_aur_template
#   tags: [saas-linux-medium-amd64]
#   stage: .pre
#   variables:
#     PACKAGE: "petsc-complex"

#################################
# * A Python module for distributed just-in-time shared library building (stable)
#################################

python-dijitso:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    PACKAGE: "python-dijitso"

#################################
# * The Python interface of FEniCS, providing a consistent PSE (Problem Solving Environment) for ordinary and partial differential equations (stable)
#################################

python-dolfin:
  extends: .build_aur_template
  # tags: [saas-linux-medium-amd64]
  stage: .pre
  allow_failure: true
  variables:
    ISARCH4EDU: "true"
    PACKAGE: "python-dolfin"
    ISDEPENDS: "true"
    DEPENDENCIES_PACKAGE: "petsc-*.pkg.tar.zst python-dijitso-*.pkg.tar.zst python-fiat-*.pkg.tar.zst python-ufl-*.pkg.tar.zst python-ffc-*.pkg.tar.zst dolfin-*.pkg.tar.zst" # scotch-*.pkg.tar.zst
  # before_script:
  #   - sudo pacman -Sy cmake --noconfirm # python-mpi4py
  #   - rm "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/petsc-*.pkg.tar.zst
  #   - ls "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"
  #   - sudo pacman -U --needed "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/petsc-*.pkg.tar.zst --noconfirm #>/dev/null 2>&1
  #   - source /etc/profile.d/petsc.sh
  #   - echo $PYTHONPATH
  needs:
    [
      "petsc",
      "python-dijitso",
      "python-fiat",
      "python-ufl",
      "python-ffc",
      "dolfin",
    ]

#################################
# * A compiler for finite element variational forms (stable)
#################################

python-ffc:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    PACKAGE: "python-ffc"
    ISDEPENDS: "true"
    DEPENDENCIES_PACKAGE: "python-dijitso-*.pkg.tar.zst python-fiat-*.pkg.tar.zst python-ufl-*.pkg.tar.zst"
  needs: ["python-dijitso", "python-fiat", "python-ufl"]

#################################
# * Supports generation of arbitrary order instances of the Lagrange elements on lines, triangles, and tetrahedra (stable)
#################################

python-fiat:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    PACKAGE: "python-fiat"

#################################
# * Simulate Functional Mockup Units (FMUs) in Python
#################################

python-fmpy:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    ISARCH4EDU: "true"
    PACKAGE: "python-fmpy"

#################################
# * Mesh generation component of FEniCS (python interface) (stable)
#################################

python-mshr:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    ISARCH4EDU: "true"
    PACKAGE: "python-mshr"
    ISDEPENDS: "true"
    DEPENDENCIES_PACKAGE: "petsc-*.pkg.tar.zst python-dijitso-*.pkg.tar.zst python-fiat-*.pkg.tar.zst python-ufl-*.pkg.tar.zst python-ffc-*.pkg.tar.zst dolfin-*.pkg.tar.zst python-dolfin-*.pkg.tar.zst mshr-*.pkg.tar.zst" # scotch-*.pkg.tar.zst
  needs:
    [
      "petsc",
      "python-dijitso",
      "python-fiat",
      "python-ufl",
      "python-ffc",
      "dolfin",
      "python-dolfin",
      "mshr",
    ]

#################################
# Take it from arch4edu repo
# * Easy, portable file locking API
#################################

# python-portalocker:
#   extends: .build_aur_template
#   tags: [saas-linux-medium-amd64]
#   stage: .pre
#   variables:
#     PACKAGE: "python-portalocker"

#################################
# Build from local PKGBUILD
#################################

python-pyvtk:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    ISAUR: "false"
    MODULE: "python-pyvtk"

#################################
# * Python bindings to the triangle library
#################################

python-triangle:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    PACKAGE: "python-triangle"

#################################
# Build from local PKGBUILD
#################################

python-ufl:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    PACKAGE: "python-ufl"

#################################
# Take it from arch4edu repo
# * Software package and libraries for graph, mesh and hypergraph partitioning, static mapping, and sparse matrix block ordering
#################################

# scotch:
#   extends: .build_aur_template
#   tags: [saas-linux-medium-amd64]
#   stage: .pre
#   variables:
#     PACKAGE: "scotch"

#################################
# * A Mesh and Field I/O Library and Scientific Database
#################################

silo:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  allow_failure: true
  variables:
    PACKAGE: "silo"

#################################
# Build from local PKGBUILD
#################################

sionlib:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    ISAUR: "false"
    MODULE: "sionlib"

#################################
# A flexible package manager for supercomputer that supports multiple versions, configurations, platforms, and compilers
#################################

spack:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    PACKAGE: "spack"

#################################
# * SParse Object Oriented Linear Equations Solver
#################################

spooles:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    PACKAGE: "spooles"

#################################
# * An Open-Source Suite for Multiphysics Simulation and Design
#################################

su2:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    PACKAGE: "su2"

#################################
# Take it from arch4edu repo
# * Distributed memory, MPI based SuperLU
#################################

# superlu_dist:
#   extends: .build_aur_template
#   tags: [saas-linux-medium-amd64]
#   stage: .pre
#   variables:
#     PACKAGE: "superlu_dist"

#################################
# * C++ library manager for Windows, Linux, and MacOS
#################################

vcpkg-git:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    PACKAGE: "vcpkg-git"

#################################
# * Parallel Partitioning, Load Balancing and Data-Management Services
#################################

zoltan:
  extends: .build_aur_template
  tags: [saas-linux-medium-amd64]
  stage: .pre
  variables:
    ISARCH4EDU: "true"
    PACKAGE: "zoltan"
    ISDEPENDS: "true"
    DEPENDENCIES_PACKAGE: "parmetis-git-*.pkg.tar.zst" # scotch-*.pkg.tar.zst
  needs: ["parmetis-git"] # "scotch"

#################################
# Create custom repository
#################################

dune-archiso-core_repo:
  extends: .load_repository_template
  tags: [saas-linux-medium-amd64]
  stage: load_repository
  dependencies: [
      "adol-c",
      # "alberta",
      # "alberta-git",
      "arpackpp",
      "calculix-ccx",
      "ccx2paraview",
      # "coin-or-ipopt",
      "colpack",
      "dolfin",
      # "duma",
      # "electricfence",
      "gstat",
      # "hypre",
      "kokkos",
      "libsnl-svn",
      "meschach",
      # "metis",
      "mshr",
      # "mtl4-svn",
      # "parmetis",
      "parmetis-git",
      "petsc",
      # "petsc-complex",
      "python-dijitso",
      "python-dolfin",
      "python-ffc",
      "python-fiat",
      "python-fmpy",
      "python-mshr",
      # "python-portalocker",
      "python-pyvtk",
      "python-triangle",
      "python-ufl",
      # "scotch",
      "silo",
      "sionlib",
      "spack",
      "spooles",
      "su2",
      # "superlu_dist",
      "vcpkg-git",
      "zoltan",
    ]

###############################
# Upload to GitLab repo (pages)
###############################

pages:
  extends: .pages_template
  tags: [saas-linux-medium-amd64]
  stage: deploy
  variables:
    TARGET_FOLDER: "x86_64"
  needs: ["dune-archiso-core_repo"]
